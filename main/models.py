from django.db import models
from datetime import datetime
# Create your models here.

class School(models.Model):
    name = models.CharField(max_length=100, blank=False)
    adress = models. TextField(blank=True)
    created_date = models.DateTimeField(default=datetime.now())
    image = models.ImageField(upload_to='Schools image', null=True, blank=True)

    def __str__(self):
        return self.name


class Teachers(models.Model):
    name = models.CharField(max_length=100, blank=True)
    age = models.IntegerField(blank=True)
    bio = models.TextField(max_length=400, blank=True)
    image = models.ImageField(upload_to='Teachers image', null=True, blank=True)
    school = models.ForeignKey(School, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=100, blank=True)
    from_class = models.IntegerField(blank=True)
    bio = models.TextField(max_length=400, blank=True)
    image = models.ImageField(upload_to='Students image', null=True, blank=True)
    teacher = models.ForeignKey(Teachers, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Directors(models.Model):
    name = models.CharField(max_length=100, blank=True)
    age = models.IntegerField(blank=True)
    bio = models.TextField(max_length=400, blank=True)
    image = models.ImageField(upload_to='Directors image', null=True, blank=True)
    school = models.OneToOneField(School, on_delete=models.CASCADE)

    def __str__(self):
        return self.name