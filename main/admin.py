from django.contrib import admin
from main.models import School, Teachers, Directors, Student
# Register your models here.

admin.site.register(School)
admin.site.register(Teachers)
admin.site.register(Directors)
admin.site.register(Student)

